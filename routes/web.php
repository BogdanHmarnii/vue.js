<?php


Route::get('/', function () {
    return view('welcome');
});

Route::get('skills', function(){
    return ['Laravel', 'Vue', 'PHP', 'Tools'];
});
Route::get('projects/create','ProjectsController@getIndex')->where('action', '[A-Za-z-0-9]+');
Route::post('projects','ProjectsController@store')->where('action', '[A-Za-z-0-9]+');

