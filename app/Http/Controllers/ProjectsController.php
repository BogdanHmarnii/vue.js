<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ProjectsController extends Controller
{
    public function getIndex()
    {
        return view('create');
    }
    public function store()
    {
        $this->validate(request(),[
        'name'=>'required',
        'description'=>'required'
        ]);
        $name=request('name');
        $description=request('description');

        return ['message'=>'Project Created'];
    }
}
