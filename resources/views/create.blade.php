<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel Vue.js</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.0/css/bulma.min.css">
</head>
<body>
<div id="app" class="container">
    <form method="POST" action="/projects" @submit.prevent='onSubmit' @keydown="form.errors.clear($event.target.name)">
    <div class="control">
        <label for="name" class="label">Project name:</label>

        <input type="text" id="name" name="name" class="input" v-model="form.name">
        <span class="help is-danger" v-if="form.errors.has('name')" v-text="form.errors.get('name')"></span>
    </div>

    <div class="control">
        <label for="description" class="label">Project description:</label>

        <input type="text" id="description" name="description" class="input" v-model="form.description">

        <span class="help is-danger" v-if="form.errors.has('description')" v-text="form.errors.get('description')"></span>
    </div>

    <div class="control">
        <button class="button is-primary" :disabled="form.errors.any()">Create</button>
    </div>


    </form>
</div>


<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="/js/app.js"></script>
</body>
</html>
